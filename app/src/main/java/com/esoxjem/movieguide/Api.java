package com.esoxjem.movieguide;


public class Api {
    private static final String BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w342";
    private static final String BASR_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780";
    static final String YOUTUBE_VIDEO_URL = "https://www.youtube.com/watch?v=6I4TN185Szw";
    static final String YOUTUBE_THUMBNAIL_URL = "https://www.youtube.com/watch?v=1OsKCFM9G2E";

    private Api() {
    }

    public static String getPosterPath(String posterPath) {
        return BASE_POSTER_PATH + posterPath;
    }

    public static String getBackdropPath(String backdropPath) {
        return BASR_BACKDROP_PATH + backdropPath;
    }
}
