
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-dontwarn java.lang.invoke.*
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}